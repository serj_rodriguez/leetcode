/*
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]
 

Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
 

Follow-up: Can you come up with an algorithm that is less than O(n2) time complexity?
*/

import UIKit

class Solution {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        for index in 0...nums.count-2 {
            let nextIndex = index + 1
            let baseNumber = nums[index]
            for nextNumberIndex in nextIndex..<nums.count {
               let sumNumber = nums[nextNumberIndex]
               let sum = baseNumber + sumNumber
               if sum == target {
                   return [index, nextNumberIndex]
               }
            }
        }
        return []
    }
}

class ElegantSolution {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        var map = [Int: Int]()
        for (i, n) in nums.enumerated() {
            let diff = target - n
            if let j = map[diff] {
                return [j, i]
            }
            map[n] = i
        }
        return []
    }
}

let solution = Solution()
print(solution.twoSum([2,7,11,15], 9))

let elegantSolution = ElegantSolution()
print(elegantSolution.twoSum([2,7,11,15], 9))
