/*
 Longest substring without repeating characters
 
 Given a string s, find the length of the longest
 substring
  without repeating characters.

 Example 1:

 Input: s = "abcabcbb"
 Output: 3
 Explanation: The answer is "abc", with the length of 3.
 Example 2:

 Input: s = "bbbbb"
 Output: 1
 Explanation: The answer is "b", with the length of 1.
 Example 3:

 Input: s = "pwwkew"
 Output: 3
 Explanation: The answer is "wke", with the length of 3.
 Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

import UIKit

class LongestStringWithoutRepeting {
    func lengthOfTheLongestSubstring(_ s: String) -> Int {
        var substringChars: [Character] = []
        var longest = 0

        for c in s {
            if !substringChars.contains(c) {
                substringChars.append(c)
            } else {
                longest = max(longest, substringChars.count)
                substringChars = [c]
            }
        }
        longest = max(longest, substringChars.count)
        return longest
    }
    /*
    func lengthOfTheLongestSubstring(_ s: String) -> Int {
        var result = ""
        var possibleLongestString = ""
        
        for (index, character) in s.enumerated() {
            if result.count < possibleLongestString.count {
                result = possibleLongestString
            }
            
            if !possibleLongestString.contains("\(character)") {
                possibleLongestString = "\(possibleLongestString)\(character)"
            }
        }
        
        return result.count
    }
     */
}

var instance = LongestStringWithoutRepeting()
print(instance.lengthOfTheLongestSubstring("pwwkew"))
