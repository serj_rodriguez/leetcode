import UIKit

/*
 You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

 You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 
 Example 1:
 Input: l1 = [2,4,3], l2 = [5,6,4]
 Output: [7,0,8]
 Explanation: 342 + 465 = 807.
 
 Example 2:
 Input: l1 = [0], l2 = [0]
 Output: [0]
 
 Example 3:
 Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 Output: [8,9,9,9,0,0,0,1]
 */

public class ListNode {
    public var val: Int
    public var next: ListNode?
    public init() { self.val = 0; self.next = nil; }
    public init(_ val: Int) { self.val = val; self.next = nil; }
    public init(_ val: Int, _ next: ListNode?) { self.val = val; self.next = next; }
}

class AddTwoNumbers {
    var listNode1: ListNode?
    var listNode2: ListNode?
    
    func addTwoNumbers(_ l1: ListNode, _ l2: ListNode) -> ListNode? {
        listNode1 = l1
        listNode2 = l2
        var result = ListNode(0)
        var ptr: ListNode? = result
        
        var carry: Int = 0
        
        repeat {
            var sum = 0 + carry
            
            if let listNode1 = listNode1 {
                sum += listNode1.val
                self.listNode1 = listNode1.next ?? nil
            }
            
            if let listNode2 = listNode2 {
                sum += listNode2.val
                self.listNode2 = listNode2.next ?? nil
            }
            
            carry = sum / 10
            sum = sum % 10
            ptr?.next = ListNode(sum)
            ptr = ptr?.next ?? nil
            
        } while listNode1 != nil || listNode2 != nil
        
        if carry == 1 {
            ptr?.next = ListNode(1)
        }
        
        return result.next
    }
}

let addTwoNumbersInstance = AddTwoNumbers()
let l1 = ListNode(2, ListNode(4, ListNode(3)))
let l2 = ListNode(5, ListNode(6, ListNode(4)))
print(addTwoNumbersInstance.addTwoNumbers(l1, l2) ?? ListNode(0))

/*
class Solution {
    func addTwoNumbers(_ l1: ListNode, _ l2: ListNode) -> ListNode? {
        var firstLinkedListAsArray: [Int] = []
        var secondLinkedListAsArray: [Int] = []
        
        firstLinkedListAsArray.append(getArrayFromLinkedList(l1))
        secondLinkedListAsArray.append(getArrayFromLinkedList(l2))
        
        print(firstLinkedListAsArray)
        print(secondLinkedListAsArray)
        /*
        // Ordering the arrays because they´re reversed
        let firstLinkedListOrdered = l1.reversed()
        let secondLinkedListOrdered = l2.reversed()
        var sumLinkedList: [Int] = []
        
        // Converting each array into a single Int
        guard let firstNumber = convertArrayToInt(Array(firstLinkedListOrdered)),
              let secondNumber = convertArrayToInt(Array(secondLinkedListOrdered))
        else {
            return nil
        }
        
        let result: Int = firstNumber + secondNumber
        let resultString = String(result)
        for numberValue in resultString {
            sumLinkedList.append(numberValue.wholeNumberValue ?? 0)
        }
        
        return sumLinkedList.reversed()
         */
        
        return nil
    }
    
    func convertArrayToInt(_ array: [Int]) -> Int? {
        var helperString = ""
        array.map { helperString = "\(helperString)\($0)" }
        return Int(helperString)
    }
    
    func getArrayFromLinkedList(_ linkedList: ListNode) -> Int {
        repeat {
            if let nextValue = linkedList.next {
                getArrayFromLinkedList(nextValue)
            }
            return linkedList.val
        } while linkedList.next != nil
    }
}

let solutionInstance = Solution()
let listNode1 = ListNode(2, ListNode(4, ListNode(3)))
let listNode2 = ListNode(5, ListNode(6, ListNode(4)))
print(solutionInstance.addTwoNumbers(listNode1, listNode2) ?? ListNode(0))
*/
