import UIKit

func fibonacci(n: Int) {

    var num1 = 0
    var num2 = 1
    
    var fibonacciSeries: [Int] = []
    fibonacciSeries.append(num1)
    fibonacciSeries.append(num2)

    for _ in 0 ..< n {
    
        let num = num1 + num2
        num1 = num2
        num2 = num
        fibonacciSeries.append(num)
    }
    
    print("result = \(num2)")
    print("Fibonnacci series of \(n) is equal to \(fibonacciSeries)")
}

fibonacci(n: 25)
